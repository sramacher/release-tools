## vim:set et ts=4 sw=4:
#
# security.py: read DSA information from a pregenerated DB and create
#              comments files
#
# (C) Copyright 2009 Philipp Kern <pkern@debian.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301  USA.

from __future__ import with_statement
import os.path

class BugsFactory():
    def __init__(self, config, section):
        self.bugs_base = config.get('bugs', 'directory')

        self.base_suite = config.get(section, 'base_suite')
        self.target_suite = section

        self.bugs = dict()
        self._load_bugs(self.base_suite)
        self._load_bugs(self.target_suite)

    def _load_bugs(self, suite):
        """
        The bugs input files we read are stored in the directory defined
        in the config file (section `base', key `directory') with the name
        `suite-nr'.  In it there's one line per buggy source package of
        the format `srcpkg 123456 234567'.
        """

        filename = os.path.join(self.bugs_base, '%s-nr' % suite)
        bugs = dict()
        with open(filename, 'r') as f:
            for line in f:
                entry = line.strip().split(' ')
                srcpkg, bug_list = entry[0], set(entry[1:])
                bugs[srcpkg] = bug_list
        self.bugs[suite] = bugs

    def get_closed_bugs(self, srcpkg):
        """Return all bugs closed in a srcpkg comparing base with target."""
        return self._compare_bugs(srcpkg, self.base_suite, self.target_suite)

    def get_opened_bugs(self, srcpkg):
        """Return all bugs opened in a srcpkg comparing base with target."""
        return self._compare_bugs(srcpkg, self.target_suite, self.base_suite)

    def _compare_bugs(self, srcpkg, suite_a, suite_b):
        """
        This is a helper function to implement the bug comparision.  It
        basically models this workflow in two ways by switching the input
        order (this example is looking for closed bugs):

            if there are no bugs in base_suite: none can be closed
            if there are no bugs in target_suite: all bugs have been closed
            else: calculate the difference

        Opened bugs are just base and target suite switched.
        """
        if srcpkg not in self.bugs[suite_a]:
            return set()
        suite_a_bugs = self.bugs[suite_a][srcpkg]
        if srcpkg not in self.bugs[suite_b]:
            return suite_a_bugs
        suite_b_bugs = self.bugs[suite_b][srcpkg]
        return suite_a_bugs - suite_b_bugs

