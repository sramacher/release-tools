## vim:set et ts=4 sw=4:
#
# lintian.py: run lintian against packages
#
# (C) Copyright 2016-2019 Adam D. Barratt <adam@adam-barratt.org.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301  USA.
#
# TODO:
# - HTML support?
# - full versus filtered diff support?

from __future__ import with_statement, print_function
import gzip
import logging
import os.path
import re
import sys
import shutil
import tempfile
from subprocess import Popen, PIPE, STDOUT
from debrelease import create_symlinks, FileNotFoundException, locate_file, strip_epoch


class LintianFactory():
    def __init__(self, projectb, config=None, section=None):
        self.projectb = projectb
        self.logger = logging.getLogger('debrelease.lintian')
        self.logger.addHandler(logging.NullHandler())
        if config is not None:
            if section:
                self.section = section
                self.pool = config.get('dak', 'pool')
                self.extra_pools = config.get('dak', 'extra_pools').split(' ')
                self.queue_directory = config.get(section, 'directory')
                self.suite = config.get(section, 'base_suite')
                self.overlay_suite = section
                self.policy_suite = config.get(section, 'policy_suite', fallback=None)
                self.diff_output = config.get(section, 'lintian_output')
                if not os.path.exists(self.diff_output):
                    os.mkdir(self.diff_output)
                assert os.path.isdir(self.diff_output)
                self.lintian_include_directory = config.get(section, 'lintian_include_directory', fallback=None)

    def diff_filename(self, srcpkg, version, arch, compressed):
        filever = strip_epoch(version)
        filename = "%s/%s_%s_%s.lintian.debdiff" % (self.diff_output, srcpkg, filever, arch)
        if compressed:
            filename = "%s.gz" % (filename)
        return filename

    def check_files(self, srcpkg, files):
        return LintianCheck(
            suite=self.suite,
            srcpkg=srcpkg,
            pool=self.pool,
            extra_pools=self.extra_pools,
            files=files,
            queue_directory=self.queue_directory,
            include_directory=self.lintian_include_directory,
        )

    def create_diff_file(self, srcpkg, version, srcver, arch, oldfiles=None, newfiles=None, against_overlay=False,
                         compressed=False):
        self.logger.debug("Checking for existing lintian diff for %s_%s/%s", srcpkg, srcver, arch)
        # check for an uncompressed file first
        filename = self.diff_filename(srcpkg, version, arch, False)
        if os.path.exists(filename):
            return None
        if compressed:
            filename = self.diff_filename(srcpkg, version, arch, compressed)
            if os.path.exists(filename):
                return None
        self.logger.debug("Generating lintian diff for %s_%s/%s", srcpkg, srcver, arch)
        diffobj = self.create_diff(srcpkg, version, arch, srcver, oldfiles, newfiles, against_overlay)
        if diffobj is None:
            return None
        diffobj.create_diff_file(filename, compressed)
        return filename

    def create_diff(self, srcpkg, version, arch, srcver=None, oldfiles=None, newfiles=None, against_overlay=False):
        self.logger.debug("Preparing to generate linitan diff for %s_%s/%s", srcpkg, version, arch)
        overlay_version = self.projectb.get_version(self.overlay_suite, srcpkg, epoch=True)
        stable_version = self.projectb.get_version(self.suite, srcpkg, epoch=True)
        if srcver is None:
            source_version = version
        else:
            source_version = srcver
        if against_overlay:
            base_version = overlay_version
            base_suite = self.overlay_suite
        else:
            base_version = stable_version
            base_suite = self.suite
        # Force overlay if not in base.
        if base_version is None:
            base_version = overlay_version
        if source_version == stable_version and version == source_version:
            # Diffing against the base suite won't work.
            return None
        if base_version == source_version and version == source_version:
            return None
        target_suite = None
        if base_version == overlay_version:
            target_suite = self.policy_suite
        elif base_version == stable_version:
            if version == overlay_version:
                target_suite = self.overlay_suite
            else:
                target_suite = self.policy_suite
        return LintianDiff(
            suite=base_suite,
            target_suite=target_suite,
            srcpkg=srcpkg,
            architecture=arch,
            base_version=base_version,
            target_version=source_version,
            pool=self.pool,
            extra_pools=self.extra_pools,
            projectb=self.projectb,
            old_files=oldfiles,
            new_files=newfiles,
            include_directory=self.lintian_include_directory,
            queue_directory=self.queue_directory,
            cache_directory=self.diff_output,
            stable_version=stable_version,
            overlay_version=overlay_version,
        )


class LintianDiff():
    def __init__(self, suite, target_suite, srcpkg, target_version, base_version,
                 pool, extra_pools=None, old_files=None, new_files=None,
                 projectb=None,
                 architecture=None, cache_directory=None, include_directory=None,
                 queue_directory=None, stable_version=None, overlay_version=None):
        self.logger = logging.getLogger('debrelease.lintian')
        self.logger.addHandler(logging.NullHandler())
        self.projectb = projectb
        self.suite = suite
        if target_suite is None:
            self.target_suite = suite
        else:
            self.target_suite = target_suite
        self.srcpkg = srcpkg
        self.architecture = architecture
        self.pool = pool
        self.extra_pools = extra_pools or []
        self.project = projectb
        self.include_directory = include_directory
        self.old_files = []
        self.new_files = []
        self.queue_directory = queue_directory
        self.cache_directory = cache_directory
        self.base_version = base_version
        self.target_version = target_version
        self.version_info = {}
        if stable_version is not None:
            self.version_info['stable'] = stable_version
        if overlay_version is not None:
            self.version_info['overlay'] = overlay_version

        # confirm that all files are present
        # also store the absolute path of the file found, to avoid
        # needlessly recomputing it later
        if old_files is not None:
            for file in old_files:
                file = locate_file(file, self.queue_directory, [self.pool] + self.extra_pools)
                if not os.path.exists(file):
                    raise FileNotFoundException(file)
                else:
                    self.old_files.append(file)
        if new_files is not None:
            for file in new_files:
                file = locate_file(file, self.queue_directory, [self.pool] + self.extra_pools)
                if not os.path.exists(file):
                    raise FileNotFoundException(file)
                else:
                    self.new_files.append(file)

    def create_diff_file(self, filename, compressed=False):
        self.logger.debug("Writing lintian diff to file")
        if compressed:
            f = gzip.open(filename, 'w')
        else:
            f = open(filename, 'wb')
        try:
            self._print_header(f)
            self.write_to_file(f)
            f.close()
        except:
            f.close()
            os.unlink(filename)

    def _print_header(self, f):
        if 'stable' in self.version_info:
            f.write(("Version in base suite: %s\n" % self.version_info['stable']).encode('ascii'))
        if 'overlay' in self.version_info:
            if self.version_info['overlay'] is None:
                version = "(not present)"
            else:
                version = self.version_info['overlay']
            f.write(("Version in overlay suite: %s\n" % version).encode('ascii'))
            f.write(b"\n")
        f.write(("Base version: %s_%s\n" % (self.srcpkg, self.base_version)).encode('ascii'))
        f.write(("Target version: %s_%s\n" % (self.srcpkg, self.target_version)).encode('ascii'))

    def write_to_file(self, file):
        file.write(self.diff)

    @property
    def diff(self):
        if not hasattr(self, '_diff'):
            self._diff = self._generate_diff()
        return self._diff

    def _generate_diff(self):
        self.logger.info("Generating lintian diff for %s_%s/%s vs %s_%s/%s",
                         self.srcpkg, self.target_version, self.architecture,
                         self.srcpkg, self.base_version, self.architecture)
        _diff = []
        _header = []
        self.logger.debug("Running lintian")
        (old_issues, old_files) = LintianCheck(self.suite, self.srcpkg, self.pool, self.extra_pools,
                                               self.projectb, self.old_files, self.base_version, self.architecture,
                                               self.include_directory, self.queue_directory, self.cache_directory).result()
        (new_issues, new_files) = LintianCheck(self.target_suite, self.srcpkg, self.pool, self.extra_pools,
                                               self.projectb, self.new_files, self.target_version, self.architecture,
                                               self.include_directory, self.queue_directory, self.cache_directory).result()
        self.logger.debug("Comparing lintian outputs")
        for gone_issue in (issue for issue in old_issues if issue not in new_issues):
            _diff.append('-%s' % (' '.join(gone_issue)))
        for new_issue in (issue for issue in new_issues if issue not in old_issues):
            _diff.append('+%s' % (' '.join(new_issue)))
        if not _diff:
            _diff.append('No differences found.')
        _diff.append('')

        if old_files:
            _header.append("Base files: %s" % (' '.join(sorted(os.path.basename(file) for file in old_files))))
        else:
            _header.append("Base files: (none)")
        if new_files:
            _header.append("Target files: %s" % (' '.join(sorted(os.path.basename(file) for file in new_files))))
        else:
            _header.append("Target files: (none)")
        _header.append('')

        return '\n'.join(_header + _diff).encode('utf-8')


class LintianFailedException(Exception):
    def __init__(self, files, error):
        self.files, self.error = \
            files, error
        super().__init__("Lintian(%s,) failed:\n%s" % (self.files, self.error))


class LintianCheck():
    temp_dir = None

    def __init__(self, suite, srcpkg,
                 pool, extra_pools=None,
                 projectb=None, files=None,
                 version=None, architecture=None,
                 include_directory=None,
                 queue_directory=None,
                 cache_directory=None):
        self.logger = logging.getLogger('debrelease.lintian')
        self.logger.addHandler(logging.NullHandler())
        self.suite = suite
        self.debug_suite = "%s-debug" % (self.suite, )
        self.projectb = projectb
        self.pool = pool
        self.extra_pools = extra_pools or []
        self.version = version
        self.package = srcpkg
        self.architecture = architecture
        self.include_directory = include_directory
        self.queue_directory = queue_directory
        self.cache_directory = cache_directory

        if self._read_from_cache_file():
            return

        if not files:
            self.logger.debug("Determining files for %s_%s/%s", srcpkg, version, architecture)
            if architecture == 'source':
                files = [self.projectb.get_dsc_file(srcpkg, version)]
            else:
                files = (pkg.filename for pkg in self.projectb.get_binaries_for_version(srcpkg, version) if
                         pkg.arch_string == architecture and
                         (self.suite is None or pkg.suite in [self.suite, self.debug_suite]))

        _files = []
        self.logger.debug("Locating files for %s_%s/%s", srcpkg, version, architecture)
        for filename in sorted(files):
            filename = locate_file(filename, self.queue_directory, [self.pool] + self.extra_pools)
            if filename.endswith('.dsc'):
                self.temp_dir = tempfile.mkdtemp()
                create_symlinks(filename, self.temp_dir,
                                [self.pool] + self.extra_pools, self.projectb)
                _files.append(os.path.join(self.temp_dir, os.path.basename(filename)))
            else:
                _files.append(filename)
        self.files = _files

        # confirm that all files are present
        for file in self.files:
            if not os.path.exists(file):
                raise FileNotFoundException(file)

    def __del__(self):
        if self.temp_dir is not None:
            shutil.rmtree(self.temp_dir)

    def _invoke_lintian(self):
        if not self.files:
            return None

        self.logger.debug("Invoking lintian with arguments:")
        # fullewi is technically an experimental output format, but it's
        # unlikely to change and provides an output that has the same
        # field layout for both source and binary output
        args = ['lintian']

        if self.include_directory:
            args.extend(['--include-dir', self.include_directory])

        args.extend([
                '--no-user-dirs', '--no-cfg',
                '--color', 'never', '--no-override', '--exp-output', 'format=fullewi',
                '--suppress-tags', 'source-nmu-has-incorrect-version-number',
                '--suppress-tags', 'changelog-should-mention-nmu',
                '--suppress-tags', 'qa-upload-has-incorrect-version-number',
                '--suppress-tags', 'maintainer-upload-has-incorrect-version-number',
                '--suppress-tags', 'team-upload-has-incorrect-version-number',
                ])
        if re.match(r'^.*~(?:deb\d+u\d+|bpo\d+\+\d+)_\w+\.u?deb$', self.files[0]):
            args.extend(['--suppress-tags', 'latest-debian-changelog-entry-without-new-version'])
        args.extend(self.files)
        self.logger.debug(args)
        invoke = Popen(args, stdout=PIPE, stderr=STDOUT, close_fds=True)
        output = invoke.communicate()
        if invoke.returncode not in [0, 1]:
            raise LintianFailedException(' '.join(self.files), output[0].strip())

        return output[0]

    def _process_results(self, results):
        # W: chirp binary (0.4.0-1) [amd64]: desktop-mime-but-no-exec-code usr/share/applications/chirp.desktop
        if results is None:
            return []

        self.logger.debug("Processing lintian output")
        _results = []
        for result in results.decode().split('\n'):
            if result == '':
                continue
            match = re.match(r'^(.:) (?:[^ ]+?) (?:[^ ]+?) \((?:[^ ]+?)\) \[(?:[^ ]+?)\]: ([^ ]+?)( (.*?))?$', result)
            if not match:
                print('Unexpected lintian output processing [%s]: %s'
                      % (' '.join(self.files), result), file=sys.stderr)
            elif match.group(4):
                if match.group(2).endswith('-address-causes-mail-loops-or-bounces') and \
                   match.group(4).endswith('@packages.debian.org>'):
                    pass
                else:
                    _results.append((match.group(1), match.group(2), match.group(4)))
            else:
                _results.append((match.group(1), match.group(2)))
        return sorted(_results)

    def result(self):
        if not hasattr(self, '_result'):
            self._result = self._process_results(self._invoke_lintian())
            self._write_cache_file()
        return (self._result, self.files)

    def cache_filename(self):
        filever = strip_epoch(self.version)
        filename = "%s/%s_%s_%s.lintian.cache" % (self.cache_directory,
                                                  self.package,
                                                  filever,
                                                  self.architecture)
        return filename

    def _write_cache_file(self):
        if self.cache_directory:
            # No existence check, as the cache should always contain the
            # results from the most recent check
            with open(self.cache_filename(), 'wb') as cache_file:
                cache_file.write(b'\n'.join([file.encode('ascii')
                                             for file in self.files]))
                cache_file.write(b'\n')
                cache_file.write(b'\n')
                cache_file.write(b'\n'.join([' '.join(result).encode('ascii')
                                             for result in sorted(self._result)]))
                cache_file.write(b'\n')

    def _read_from_cache_file(self):
        if not self.cache_directory:
            return False
        if not os.path.exists(self.cache_filename()):
            return False
        with open(self.cache_filename()) as cache_file:
            in_files = True
            files = []
            results = []
            for line in cache_file:
                line = line.strip()
                if line == '':
                    in_files = False
                elif in_files:
                    files.append(line)
                else:
                    results.append(tuple(line.split(' ', 2)))
        self.files = files
        self._result = results
        return True
